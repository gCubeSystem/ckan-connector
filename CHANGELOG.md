# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.4.0]

- Removed old code which set the user role in ckan [#23310]
- Fixed gcat discovery [#23309]
- Supporting new authorization  [#23306]


## [v1.3.0]

- updated gcat-client version [#21530]
- adoption of gcube-smartgears-bom.2.1.0
